package application.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

// TODO: may be needed to add an additional layer DTO:
// https://stackoverflow.com/questions/36174516/rest-api-dtos-or-not
//https://stackoverflow.com/questions/1724774/java-data-transfer-object-naming-convention/35341664#35341664
@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "company_profiles")
public class CompanyProfile implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    // This will probabily be connected to a specific list of company types.
    private String companyType;

    @NotBlank
    @Length(max = 100)
    private String address;

    @Column
    @Length(max = 100)
    private String secondaryAddress;

    @Column
    @Length(max = 32)
    private String postalCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Province province;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private City city;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Region region;

    @Column
    @NonNull
    private String nation;

    @Column
    @NonNull
    private String owner;

    @Column
    @NonNull
    private String description;

    @Column
    private Date startDate;

    @Column
    @Length(max = 15)
    private String phoneNumber;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    @Column(nullable = false)
    private Boolean removed;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    /* This constructor is used only for updateCompany */
    public CompanyProfile(@NotBlank String companyType, @NotBlank String address, @NotBlank String postalCode, Province province, City city, Region region, @NotBlank String nation, @NotBlank String owner, @NotBlank String description, Date startDate) {
        this.companyType = companyType;
        this.address = address;
        this.postalCode = postalCode;
        this.province = province;
        this.city = city;
        this.region = region;
        this.nation = nation;
        this.owner = owner;
        this.description = description;
        this.startDate = startDate;
    }
}
