package application.controller;

import application.entity.CompanyProfile;
import application.service.CompanyProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CompanyProfileController {

    @Autowired
    CompanyProfileService companyProfileService;

    @RequestMapping(value = "/company-profiles", method = RequestMethod.GET)
    public List<CompanyProfile> getCompanies() {
        return companyProfileService.getCompanyProfiles();
    }

    @RequestMapping(value = "/company-profile", method = RequestMethod.POST)
    public ResponseEntity<CompanyProfile> saveCompany(@Valid @RequestBody CompanyProfile companyProfile) {
        return companyProfileService.saveCompanyProfile(companyProfile);
    }

    @RequestMapping(value = "/company-profile/{id}", method = RequestMethod.GET)
    public ResponseEntity<CompanyProfile> getCompanyPerId(@RequestParam Long id) {
        return companyProfileService.getCompanyProfilePerId(id);
    }

    @RequestMapping(value = "/company-profile/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CompanyProfile> updateCompanyPerId(@RequestParam Long id, @RequestBody CompanyProfile companyProfileUpdates) {
        return companyProfileService.updateCompanyProfilePerId(id, companyProfileUpdates);
    }

    @RequestMapping(value = "/company-profiles", method = RequestMethod.DELETE)
    public ResponseEntity<List<CompanyProfile>> deleteCompanies() {
        return companyProfileService.deleteCompanyProfiles();
    }

    @RequestMapping(value = "/company-profile/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<CompanyProfile> deleteCompanyPerId(@RequestParam Long id) {
        return companyProfileService.deleteCompanyPerId(id);
    }

    @RequestMapping(value = "/server-response-test", method = RequestMethod.GET)
    public String response() {
        return companyProfileService.returnServerConfirmation();
    }


}
