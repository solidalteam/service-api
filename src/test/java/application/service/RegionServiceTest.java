package application.service;

import application.entity.Region;
import application.repository.RegionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
public class RegionServiceTest {

    @TestConfiguration
    static class RegionServiceContextConfiguration {

        @Bean
        public RegionService RegionService() {
            return new RegionService();
        }
    }

    @Autowired
    RegionService regionService;

    @MockBean
    RegionRepository regionRepository;

    @Before
    public void setUp() throws Exception {

        Region region = new Region();
        region.setName("Lazio");

        /** I have defined which method will be used during the test and also what kind of return. **/
        Mockito.when(regionRepository.findByName(region.getName())).thenReturn(region);
    }

    @Test
    public void getRegionByName() {
        String name = "Lazio";

        Region found = regionRepository.findByName(name);

        assertThat(found.getName()).isEqualTo(name);
    }
}