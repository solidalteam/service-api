package application.repository;

import application.entity.Region;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RegionRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    private RegionRepository regionRepository;

    @Test
    public void findRegionByName() {
        // given
        String name = "Lazio";
        Region region = new Region();
        region.setName(name);

        entityManager.persist(region);
        entityManager.flush();

        // when
        Region regionFound = regionRepository.findByName(region.getName());

        // then
        assertThat(regionFound.getName()).isEqualTo(name);
    }
}