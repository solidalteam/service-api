package application.service;

import application.entity.City;
import application.repository.CityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

/* Our Service layer code is dependent on our Repository. However, to test the Service layer, we do not need to know or
care about how the persistence layer is implemented.
Ideally, we should be able to write and test our Service layer code without wiring in our full persistence layer.

To achieve this, we can use the mocking support provided by Spring Boot Test.

To check the Service class, we need to have an instance of Service class created and available as a @Bean so that we can
 @Autowire it in our test class. This configuration is achieved by using the @TestConfiguration annotation.

During component scanning, we might find components or configurations created only for specific tests accidentally get
picked up everywhere. To help prevent that, Spring Boot provides @TestConfiguration annotation that can be used on classes
 in src/test/java to indicate that they should not be picked up by scanning.

Another interesting thing here is the use of @MockBean. It creates a Mock for the EmployeeRepository which can be used to
 bypass the call to the actual EmployeeRepository:
 */
@RunWith(SpringRunner.class)
public class CityServiceTest {

    @TestConfiguration
    static class CityServiceContextConfiguration {

        @Bean
        public CityService cityService() {
            return new CityService();
        }
    }

    @Autowired
    private CityService cityService;

    @MockBean
    private CityRepository cityRepository;

    @Before
    public void setUp() {
        City city = new City();

        city.setName("Empoli");
        city.setCadastralCode("124521");

        Mockito.when(cityRepository.findByName(city.getName()))
                .thenReturn(city);
    }

    @Test
    public void whenValidName_thenCityShouldBeFound() {
        String name = "Empoli";

        City cityFound = cityService.getCityByName(name);

        assertThat(cityFound.getName()).isEqualTo(name);

    }
}