package application.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

//Testing not working with this annotation enabled.
@Configuration
@EnableJpaAuditing
public class ApplicationConfig {
}
