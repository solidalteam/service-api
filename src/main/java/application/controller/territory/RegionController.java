package application.controller.territory;

import application.entity.Region;
import application.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class RegionController {

    @Autowired
    RegionService regionService;

    @RequestMapping(value = "/regions", method = RequestMethod.GET)
    public List<Region> getRegions() {
        return regionService.getRegions();
    }

    @RequestMapping(value = "/region/{id}", method = RequestMethod.GET)
    public ResponseEntity<Region> getRegionById(@RequestParam long id) {
        return regionService.getRegionById(id);
    }

    @RequestMapping(value = "/region/{name}", method = RequestMethod.GET)
    public Region getRegionByName(@RequestParam String name) {
        return regionService.getRegionByName(name);
    }
}
