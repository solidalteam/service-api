package application.repository;

import application.entity.City;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class CityRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    private CityRepository cityRepository;

    @Test
    public void findCityByName() {
        // Given city
        City city = new City();

        city.setName("Empoli");
        city.setCadastralCode("124521");

        entityManager.persist(city);
        entityManager.flush();

        // when
        City cityFound = cityRepository.findByName(city.getName());

        // then
        assertThat(cityFound.getName()).isEqualTo(city.getName());


    }
}