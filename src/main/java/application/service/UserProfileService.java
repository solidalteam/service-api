package application.service;

import application.entity.UserProfile;
import application.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.*;

@Service("userProfileService")
public class UserProfileService {

    @Autowired
    UserProfileRepository userProfileRepository;

    public List<UserProfile> getUserProfiles() {
        return userProfileRepository.findAll();
    }

    public ResponseEntity<UserProfile> saveUserProfile(UserProfile user) {
        if (user != null) {
            userProfileRepository.save(user);
            return ok().body(user);
        } else {
            return noContent().build();
        }
    }

    public ResponseEntity<UserProfile> updateUserProfilePerId(Long id, UserProfile userUpdates) {
        Optional<UserProfile> user = userProfileRepository.findById(id);

        if (user.isPresent()) {
//            updateUserProfilePerId(userUpdates);
            return ok().body(user.get());
        } else {
            user.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }

    public ResponseEntity<UserProfile> getUserProfilePerId(Long id) {
        Optional<UserProfile> userProfile = userProfileRepository.findById(id);

        if (userProfile.isPresent()) {
            return ok().body(userProfile.get());
        } else {
            userProfile.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }    

    public ResponseEntity<UserProfile> deleteUserProfilePerId(Long id) {
        Optional<UserProfile> userProfile = userProfileRepository.findById(id);

        if (userProfile.isPresent()) {
            userProfile.get().setRemoved(true);

            return ok().build();
        } else return notFound().build();
    }

//    public UserProfile updateUserProfilePerId(UserProfile user) {
//        return new UserProfile(user.getName(), user.getDescription(), user.getPrice(), user.getCategory());
//    }

}
