package application;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import application.controller.CategoryController;
import application.controller.CompanyProfileController;
import application.controller.ProductController;
import application.service.CategoryService;
import application.service.CompanyProfileService;
import application.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SolidalApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    // Controllers
    @Autowired
    CategoryController categoryController;

    @Autowired
    CompanyProfileController companyProfileController;

    @Autowired
    ProductController productController;

    // Services
    @Autowired
    CategoryService categoryService;

    @Autowired
    CompanyProfileService companyProfileService;

    @Autowired
    ProductService productService;

    // SANITY CHECK TEST
    @Test
    public void contextLoads() throws Exception {
        assertThat(categoryController).isNotNull();
        assertThat(companyProfileController).isNotNull();
        assertThat(productController).isNotNull();
        assertThat(categoryService).isNotNull();
        assertThat(companyProfileService).isNotNull();
        assertThat(productService).isNotNull();
    }

    /*
    We do not start the server at all, but test only the layer below that, where Spring handles
    the incoming HTTP request and hands it off to the controller. That way, almost the full stack is used, and the
    code will be called exactly the same way as if it was processing a real HTTP request, but without the cost of
    starting the server.
     */

    @Test
    public void shouldReturnResponse() throws Exception {
        this.mockMvc.perform(get("/api/server-response-test").header("Origin", "*")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Request received.")));
    }

    @Test
    public void testGetCompanies() throws Exception {
        this.mockMvc.perform(get("/api/companies").header("Origin", "*")).andDo(print()).andExpect((status().isOk()))
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

}