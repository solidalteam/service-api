package application.service;

import application.entity.Province;
import application.repository.ProvinceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ProvinceServiceTest {

    // Specific context creation on test behalf.
    @TestConfiguration
    static class ProvinceServiceContextConfiguration {

        @Bean
        public ProvinceService provinceService() { return new ProvinceService();}
    }

    @Autowired
    private ProvinceService provinceService;

    @MockBean
    private ProvinceRepository provinceRepository;

    @Before
    public void setUp() throws Exception {
        String name = "Como";

        Province province = new Province();
        province.setName(name);

        Mockito.when(provinceRepository.findProvinceByName(name)).thenReturn(province);
    }

    @Test
    public void findProvinceByName() {
        String name = "Como";
        Province province = new Province();
        province.setName(name);

        assertThat(provinceRepository.findProvinceByName(name)).isEqualTo(province);
    }
}