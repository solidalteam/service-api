package application.service;

import application.entity.Region;
import application.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service("regionService")
public class RegionService {

    @Autowired
    RegionRepository regionRepository;

    public List<Region> getRegions() {
        return regionRepository.findAll();
    }

    public ResponseEntity<Region> getRegionById(long id) {
        Optional<Region> region = regionRepository.findById(id);

        if (region.isPresent()) {
            return ResponseEntity.ok().build();
        } else {
            region.orElseThrow(NoSuchElementException::new);
            return ResponseEntity.notFound().build();
        }
    }

    public Region getRegionByName(String name) {
        return regionRepository.findByName(name);
    }
}
