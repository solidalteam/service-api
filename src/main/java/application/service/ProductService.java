package application.service;

import application.entity.Product;
import application.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@Service("productService")
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    public ResponseEntity<Product> saveProduct(Product product) {
        if (product != null) {
            productRepository.save(product);
            return ok().body(product);
        } else {
            return noContent().build();
        }
    }

    public ResponseEntity<Product> updateProduct(Long id, Product productUpdates) {
        Optional<Product> product = productRepository.findById(id);

        if (product.isPresent()) {
            updateProduct(productUpdates);
            return ok().body(product.get());
        } else {
            product.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }

    public Product updateProduct(Product product) {
        return new Product(product.getName(), product.getDescription(), product.getPrice(), product.getCategory());
    }

}
