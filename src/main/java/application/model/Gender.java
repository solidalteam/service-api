package application.model;

public enum Gender {
    MALE, FEMALE
}