package application.controller;

import application.entity.UserProfile;
import application.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserProfileController {

    @Autowired
    UserProfileService userProfileService;

    @RequestMapping(value = "/user-profiles", method = RequestMethod.GET)
    public List<UserProfile> getUserProfiles() {
        return userProfileService.getUserProfiles();
    }

    @RequestMapping(value = "/user-profile", method = RequestMethod.POST)
    public ResponseEntity<UserProfile> saveUserProfile(@Valid @RequestBody UserProfile userProfile) {
        return userProfileService.saveUserProfile(userProfile);
    }

    @RequestMapping(value = "/userProfile/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserProfile> getUserProfilePerId(@RequestParam Long id) {
        return userProfileService.getUserProfilePerId(id);
    }

    @RequestMapping(value = "/userProfile/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserProfile> updateUserProfilePerId(@RequestParam Long id, @RequestBody UserProfile userProfileUpdates) {
        return userProfileService.updateUserProfilePerId(id, userProfileUpdates);
    }

}
