package application.controller;

import application.entity.Company;
import application.entity.CompanyProfile;
import application.service.CompanyProfileService;
import application.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @RequestMapping(value = "/companies", method = RequestMethod.GET)
    public List<Company> getCompanies() {
        return companyService.getCompanies();
    }

    @RequestMapping(value = "/company", method = RequestMethod.POST)
    public ResponseEntity<Company> saveCompany(@Valid @RequestBody Company company) {
        return companyService.saveCompany(company);
    }

    @RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
    public ResponseEntity<Company> getCompanyPerId(@RequestParam Long id) {
        return companyService.getCompanyPerId(id);
    }

    @RequestMapping(value = "/company/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Company> updateCompanyPerId(@RequestParam Long id, @RequestBody Company companyUpdates) {
        return companyService.updateCompanyPerId(id, companyUpdates);
    }

    @RequestMapping(value = "/companies", method = RequestMethod.DELETE)
    public ResponseEntity<List<Company>> deleteCompanies() {
        return companyService.deleteCompanies();
    }

    @RequestMapping(value = "/company/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Company> deleteCompanyPerId(@RequestParam Long id) {
        return companyService.deleteCompanyPerId(id);
    }



}
