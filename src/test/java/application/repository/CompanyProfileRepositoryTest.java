package application.repository;

import application.entity.City;
import application.entity.CompanyProfile;
import application.entity.Province;
import application.entity.Region;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CompanyProfileRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Test
    public void findAllCompanies() {
        // Given all companies.
        CompanyProfile companyProfile = new CompanyProfile();
        CompanyProfile companyProfile2 = new CompanyProfile();
        CompanyProfile companyProfile3 = new CompanyProfile();

        companyProfile.setId((long) 1512215);
        companyProfile2.setId((long) 252121);
        companyProfile3.setId((long) 3532);

        companyProfile.setRemoved(false);
        companyProfile2.setRemoved(false);
        companyProfile3.setRemoved(false);

        companyProfile.setPostalCode("20900");
        companyProfile2.setPostalCode("20813");
        companyProfile3.setPostalCode("20100");

        companyProfile.setOwner("Pippo");
        companyProfile2.setOwner("Caio");
        companyProfile3.setOwner("Semprogno");

        companyProfile.setNation("italy");
        companyProfile2.setNation("italy");
        companyProfile3.setNation("italy");

        companyProfile.setAddress("Via Napoli, 32");
        companyProfile2.setAddress("Via Napoli, 34");
        companyProfile3.setAddress("Via Napoli, 36");

        companyProfile.setCompanyType("S.r.l.");
        companyProfile2.setCompanyType("S.r.l.");
        companyProfile3.setCompanyType("S.r.l.");

        Province province = new Province();
        Province province2 = new Province();
        Province province3 = new Province();

        province.setName("Milano");
        province2.setName("Asd");
        province3.setName("Pug213ia");

        companyProfile.setProvince(province);
        companyProfile2.setProvince(province2);
        companyProfile3.setProvince(province3);

        entityManager.persist(province);
        entityManager.persist(province2);
        entityManager.persist(province3);

        City city = new City();
        City city2 = new City();
        City city3 = new City();

        city.setName("Milano");
        city2.setName("Roma");
        city3.setName("Lecce");

        companyProfile.setCity(city);
        companyProfile2.setCity(city2);
        companyProfile3.setCity(city3);

        Region region = new Region();
        Region region2 = new Region();
        Region region3 = new Region();

        region.setName("Lombardia");
        region2.setName("Lazio");
        region3.setName("Puglia");

        region.setId(1);
        region.setId(2);
        region.setId(3);

        companyProfile.setRegion(region);
        companyProfile.setRegion(region2);
        companyProfile.setRegion(region3);

        entityManager.persist(city);
        entityManager.persist(city2);
        entityManager.persist(city3);


        Set<CompanyProfile> createdCompanies = new HashSet<>();

        createdCompanies.add(companyProfile);
        createdCompanies.add(companyProfile2);
        createdCompanies.add(companyProfile3);

        region.setCompanies(createdCompanies);

        entityManager.persist(region);
        entityManager.persist(region2);
        entityManager.persist(region3);

        entityManager.persist(companyProfile);
        entityManager.persist(companyProfile2);
        entityManager.persist(companyProfile3);

        entityManager.flush();

        // When
        cityRepository.save(city);
        cityRepository.save(city2);
        cityRepository.save(city3);

        provinceRepository.save(province);
        provinceRepository.save(province2);
        provinceRepository.save(province3);

        regionRepository.save(region);
        regionRepository.save(region2);
        regionRepository.save(region3);

        List<CompanyProfile> foundCompanies = companyProfileRepository.findAll();

        // Then
        assertEquals(createdCompanies, foundCompanies);

    }

}