package application.controller.territory;

import application.entity.Province;
import application.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class ProvinceController {

    @Autowired
    private ProvinceService provinceService;

    @RequestMapping(value = "/province/{name}", method = RequestMethod.GET)
    public Province findProvinceByName(@RequestParam String name) {
        return provinceService.findProvinceByName(name);
    }

    @RequestMapping(value = "/provinces", method = RequestMethod.GET)
    public List<Province> getProvinces() {
        return provinceService.getProvinces();
    }

    @RequestMapping(value = "/province/{id}", method = RequestMethod.GET)
    public Optional<Province> findProvinceById(@RequestParam long id) {
        return provinceService.findProvinceById(id);
    }
}
