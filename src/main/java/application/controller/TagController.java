package application.controller;


import application.entity.Tag;
import application.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TagController {


    @Autowired
    TagService tagService;

    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public List<Tag> getTags() {
        return tagService.getTags();
    }

    @RequestMapping(value = "/tag", method = RequestMethod.POST)
    public ResponseEntity<Tag> saveTag(@Valid @RequestBody Tag tag) {
        return tagService.saveTag(tag);
    }

    @RequestMapping(value = "/tag/{id}", method = RequestMethod.GET)
    public ResponseEntity<Tag> getTagPerId(@RequestParam Long id) {
        return tagService.getTagPerId(id);
    }

    @RequestMapping(value = "/tag/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Tag> updateTagPerId(@RequestParam Long id, @RequestBody Tag tagUpdates) {
        return tagService.updateTagPerId(id, tagUpdates);
    }

    @RequestMapping(value = "/tag/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Tag> deleteTagPerId(@RequestParam Long id) {
        return tagService.deleteTagPerId(id);
    }
}
