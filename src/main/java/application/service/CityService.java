package application.service;

import application.entity.City;
import application.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("cityService")
public class CityService {

    @Autowired
    CityRepository cityRepository;

    public City getCityByName(String name) {
        return cityRepository.findByName(name);
    }

    public List<City> getCities() {
        return cityRepository.findAll();
    }

    public Optional<City> getCityById(long id) {

        return cityRepository.findById(id);
    }
}
