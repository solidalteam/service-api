package application.payload;

import application.entity.User;
import lombok.Data;

@Data
public class JwtAuthenticationResponse {

    private String accessToken;
    private String tokenType = "Bearer";
    private Object user;

    public JwtAuthenticationResponse(String accessToken, Object user) {
        this.accessToken = accessToken;
        this.user = user;
    }
}
