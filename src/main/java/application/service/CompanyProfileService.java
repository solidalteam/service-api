package application.service;

import application.entity.CompanyProfile;
import application.repository.CompanyProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.*;

@Service("companyProfileService")
public class CompanyProfileService {

    @Autowired
    CompanyProfileRepository companyProfileRepository;

    public List<CompanyProfile> getCompanyProfiles() {
        List<CompanyProfile> companies = companyProfileRepository.findAll();
        List<CompanyProfile> filteredCompaniesProfiles = new ArrayList<>();

        companies.forEach(company -> {
            if (!company.getRemoved()) {
                filteredCompaniesProfiles.add(company);
            }
        });

        return filteredCompaniesProfiles;
    }

    public ResponseEntity<CompanyProfile> saveCompanyProfile(CompanyProfile companyProfile) {
        if (companyProfile != null) {
            companyProfileRepository.save(companyProfile);
            return ok().body(companyProfile);
        } else {
            return noContent().build();
        }
    }

    public ResponseEntity<List<CompanyProfile>> deleteCompanyProfiles() {
        List<CompanyProfile> companyProfiles = companyProfileRepository.findAll();

        if (!companyProfiles.isEmpty()) {
            companyProfiles.forEach((CompanyProfile companyProfile) -> {
                if (!companyProfile.getRemoved()) {
                    companyProfile.setRemoved(true);
                }
            });

            companyProfileRepository.saveAll(companyProfiles);
        } else {
            return notFound().build();
        }

        return ok().build();
    }

    public ResponseEntity<CompanyProfile> getCompanyProfilePerId(Long id) {
        Optional<CompanyProfile> company = companyProfileRepository.findById(id);

        if (company.isPresent()) {
            return ok().body(company.get());
        } else {
            company.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }

    public ResponseEntity<CompanyProfile> updateCompanyProfilePerId(Long id, CompanyProfile companyProfileUpdates) {
        Optional<CompanyProfile> company = companyProfileRepository.findById(id);

        if (company.isPresent()) {
            updateCompanyProfiles(companyProfileUpdates);
            return ok().body(company.get());
        } else {
            company.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }

    public ResponseEntity<CompanyProfile> deleteCompanyPerId(Long id) {
        Optional<CompanyProfile> companyProfile = companyProfileRepository.findById(id);

        if (companyProfile.isPresent()) {
            companyProfile.get().setRemoved(true);

            return ok().build();
        } else return notFound().build();
    }

    public String returnServerConfirmation() {
        return "Request received.";
    }

    public CompanyProfile updateCompanyProfiles(CompanyProfile companyProfile) {
        return new CompanyProfile(companyProfile.getCompanyType(), companyProfile.getAddress(), companyProfile.getPostalCode(), companyProfile.getProvince(), companyProfile.getCity(), companyProfile.getRegion(), companyProfile.getNation(), companyProfile.getOwner(), companyProfile.getDescription(), companyProfile.getStartDate());
    }
}



