package application.controller.territory;

import application.entity.Province;
import application.entity.Region;
import application.service.ProvinceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProvinceController.class)
public class ProvinceControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProvinceService provinceService;

    @Test
    public void findProvinceByName() throws Exception {
        String name = "Como";

        Province province = new Province();
        province.setName(name);

        List<Province> provinces = Arrays.asList(province);

        given(provinceService.getProvinces()).willReturn(provinces);

        mvc.perform(get("/api/provinces").header("Origin", "*")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }
}