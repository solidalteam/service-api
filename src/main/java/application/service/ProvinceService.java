package application.service;

import application.entity.Province;
import application.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("provinceService")
public class ProvinceService {

    @Autowired
    private ProvinceRepository provinceRepository;

    public Province findProvinceByName(String name) {
        return provinceRepository.findProvinceByName(name);
    }

    public List<Province> getProvinces() {
        return provinceRepository.findAll();
    }

    public Optional<Province> findProvinceById(long id) {
        return provinceRepository.findById(id);
    }
}
