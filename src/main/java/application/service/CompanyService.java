package application.service;

import application.entity.Company;
import application.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.*;

@Service("companyService")
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public List<Company> getCompanies() {
        List<Company> companies = companyRepository.findAll();
        List<Company> filteredCompanies = new ArrayList<>();

        companies.forEach(company -> {
            if (!company.getRemoved()) {
                filteredCompanies.add(company);
            }
        });

        return filteredCompanies;
    }

    public ResponseEntity<Company> saveCompany(Company company) {
        if (company != null) {
            companyRepository.save(company);
            return ok().body(company);
        } else {
            return noContent().build();
        }
    }

    public ResponseEntity<List<Company>> deleteCompanies() {
        List<Company> companies = companyRepository.findAll();

        if (!companies.isEmpty()) {
            companies.forEach((Company company) -> {
                if (!company.getRemoved()) {
                    company.setRemoved(true);
                }
            });

            companyRepository.saveAll(companies);
        } else {
            return notFound().build();
        }

        return ok().build();
    }

    public ResponseEntity<Company> getCompanyPerId(Long id) {
        Optional<Company> company = companyRepository.findById(id);

        if (company.isPresent()) {
            return ok().body(company.get());
        } else {
            company.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }

    public ResponseEntity<Company> updateCompanyPerId(Long id, Company companyUpdates) {
        Optional<Company> company = companyRepository.findById(id);

        if (company.isPresent()) {
//            updateCompany(companyUpdates);
            return ok().body(company.get());
        } else {
            company.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }

    public ResponseEntity<Company> deleteCompanyPerId(Long id) {
        Optional<Company> company = companyRepository.findById(id);

        if (company.isPresent()) {
            company.get().setRemoved(true);

            return ok().build();
        } else return notFound().build();
    }

}



