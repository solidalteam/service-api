package application.controller.territory;

import application.entity.City;
import application.service.CityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


/**
 * Test of {@link CityController}
 */

@RunWith(MockitoJUnitRunner.class)
public class CityControllerTest {

    private MockMvc mvc;

    @Mock
    private CityService cityService;

    @InjectMocks
    private CityController cityController;

    // This object will be magically initialized by the initFields method below.
    private JacksonTester<List<City>> cityJacksonTester;

    @Before
    public void setUp() {
        // We would need this line if we would not use MockitoJUnitRunner
        // MockitoAnnotations.initMocks(this);
        // Initializes the JacksonTester
        JacksonTester.initFields(this, new ObjectMapper());
        // MockMvc standalone approach.
        mvc = MockMvcBuilders.standaloneSetup(cityController).build();
    }

    @Test
    public void canRetreiveCitiesIfExist() throws Exception {
        City city = new City();
        city.setName("Milano");

        List<City> cities = Arrays.asList(city);

        // given
        given(cityService.getCities()).willReturn(cities);

        // when
        MockHttpServletResponse response = mvc.perform(
                get("/api/cities")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(cityJacksonTester.write(cities).getJson());

    }

}