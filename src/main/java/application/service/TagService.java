package application.service;

import application.entity.Tag;
import application.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.*;

@Service("tagService")
public class TagService {

    @Autowired
    TagRepository tagRepository;

    public List<Tag> getTags() {
        return tagRepository.findAll();
    }

    public ResponseEntity<Tag> saveTag(Tag tag) {
        if (tag != null) {
            tagRepository.save(tag);
            return ok().body(tag);
        } else {
            return noContent().build();
        }
    }

    public ResponseEntity<Tag> updateTagPerId(Long id, Tag tagUpdates) {
        Optional<Tag> tag = tagRepository.findById(id);

        if (tag.isPresent()) {
//            updateTag(tagUpdates);
            return ok().body(tag.get());
        } else {
            tag.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }

    public ResponseEntity<Tag> getTagPerId(Long id) {
        Optional<Tag> tag = tagRepository.findById(id);

        if (tag.isPresent()) {
            return ok().body(tag.get());
        } else {
            tag.orElseThrow(NoSuchElementException::new);
            return notFound().build();
        }
    }

    public ResponseEntity<Tag> deleteTagPerId(Long id) {
        Optional<Tag> tag = tagRepository.findById(id);

        if (tag.isPresent()) {
            tag.get().setRemoved(true);

            return ok().build();
        } else return notFound().build();
    }

//    public Tag updateTag(Tag tag) {
//        return new Tag(tag.getName(), tag.getDescription(), tag.getPrice(), tag.getCategory());
//    }

}
