package application.controller.territory;

import application.entity.City;
import application.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class CityController {

    @Autowired
    CityService cityService;

    @RequestMapping(value = "/cities", method = RequestMethod.GET)
    public List<City> getCities() {
        return cityService.getCities();
    }

    @RequestMapping(value = "/city/{name}", method = RequestMethod.GET)
    public City getCityByName(@RequestParam String name) {
        return cityService.getCityByName(name);
    }

    @RequestMapping(value = "/city/{id}", method = RequestMethod.GET)
    public Optional<City> getCityById(@RequestParam long id) {
        return cityService.getCityById(id);
    }
}
