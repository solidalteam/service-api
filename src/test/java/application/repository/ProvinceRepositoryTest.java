package application.repository;

import application.entity.Province;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ProvinceRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Test
    public void findProvinceByName() {
        // given
        String name = "Como";

        Province province = new Province();
        province.setName(name);

        entityManager.persist(province);
        entityManager.flush();

        // when
        Province provinceFound = provinceRepository.findProvinceByName(province.getName());

        // then
        assertThat(provinceFound.getName()).isEqualTo(name);

    }
}