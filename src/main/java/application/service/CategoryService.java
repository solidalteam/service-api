package application.service;

import application.entity.Category;
import application.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

@Service("categoryService")
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    public ResponseEntity<Category> saveCategory(Category category) {
        if (category != null) {
            categoryRepository.save(category);
            return ok().body(category);
        } else {
            return noContent().build();
        }
    }

    public List<Category> getCities() {
        return categoryRepository.findAll();
    }

    public Optional<Category> getCategoryById(long id) {

        return categoryRepository.findById(id);
    }

}
